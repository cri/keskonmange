from flask import Flask, request, render_template, url_for
import datasheet

app = Flask(__name__)

def process_form():
    if "code" in request.form.keys():
        print(request.form["code"])
        d = datasheet.datasheet_from_form(request.form)
        print(d.to_json())
        with open(d.code+".json", "w") as target_file:
            target_file.write(d.to_json())
    return render_template('form.html', name=request.form['name'], date=request.form['date'])


@app.route('/', methods=['GET', 'POST'])
def receive_form():
    url_for('static', filename='form.js')
    if request.method == 'POST':
        return process_form()
    else:
        return render_template('form.html', name="")
