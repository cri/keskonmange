from enum import Enum
from copy import copy
import re
import json
# - Lien avec le dataset
#     - code denrée
# - Données
#     - Marque
#     - Société
#     - Site de fabrication
#     - Dénomination légale de ventes
#     - Ingrédients
#         - Nom
#         - Origine
#         - Contient des OGM ?
#         - Issus de l'agriculture biologique ?
#         - Issu d'une pêche responsable ?
# - Métadonnées
#     - Date de l'extraction
#     - Personne ayant extrait les infos
class FoodAttributes(Enum):
    DATE = "date"
    NAME = "name"
    CODE = "code"
    BRAND = "brand"
    FIRM = "firm"
    MANUF_SITE="manufacturing_site"
    LEGAL_NAME="legal_name"
    INGREDIENTS="ingredients"

class IngredientAttribute(Enum):
    PREFIX="ingredient"
    NAME="Name"
    ORIGIN="Origin"
    GMO="GMO"
    ORGANIC="Organic"
    FAIR_FISHING="FairFishing"

class Ingredient:
    def __init__(self, name="", origin="", has_gmo="", is_organic="", fair_fishing=""):
        self.name=name
        self.origin=origin
        self.has_gmo=has_gmo
        self.is_organic=is_organic
        self.fair_fishing=fair_fishing

    def to_map(self):
        return {
            IngredientAttribute.NAME.value:self.name, IngredientAttribute.ORIGIN.value:self.origin,
            IngredientAttribute.GMO.value:self.has_gmo,
            IngredientAttribute.ORGANIC.value:self.is_organic,
            IngredientAttribute.FAIR_FISHING.value:self.fair_fishing}

    def to_json(self):
        return json.dumps(self.to_map())

class Datasheet:
    def __init__(self, code="", brand="", company="", manufacturing_site = "", legal_name="", person="", date=""):
        self.code=code
        self.brand=brand
        self.company=company
        self.legal_name=legal_name
        self.manufacturing_site = manufacturing_site
        self.ingredients = []
        self.person = person
        self.date = date

    def add_ingredient(self, i):
        self.ingredients.append(i)

    def to_json(self):
        return json.dumps(
            {FoodAttributes.CODE.value:self.code, FoodAttributes.BRAND.value:self.brand,
            FoodAttributes.FIRM.value:self.company,
            FoodAttributes.MANUF_SITE.value:self.manufacturing_site,
            FoodAttributes.LEGAL_NAME.value:self.legal_name,
            FoodAttributes.NAME.value:self.person,
            FoodAttributes.DATE.value:self.date,
            FoodAttributes.INGREDIENTS.value:[i.to_map() for i in self.ingredients]
            }, ensure_ascii=True
        )

def datasheet_from_form(form):
    d = Datasheet()
    # This is a temporary accumulator where the retrieved values are stored before being copied to the dataset, when all the fields have been replaced (counted with counter)
    i = Ingredient()
    # This counter is used to track whenever ingredients have been completely collected, and should be added to the datasheet
    counter = 0
    for field in form.keys():
        if field == FoodAttributes.BRAND.value:
            d.brand = form[field]
        elif field == FoodAttributes.FIRM.value:
            d.company = form[field]
        elif field == FoodAttributes.CODE.value:
            d.code = form[field]
        elif field == FoodAttributes.LEGAL_NAME.value:
            d.legal_name = form[field]
        elif field == FoodAttributes.NAME.value:
            d.extractor = form[field]
        elif field == FoodAttributes.DATE.value:
            d.extractor = form[field]
        elif field.startswith(IngredientAttribute.PREFIX.value):
            counter += 1
            # If the field is for an ingredient, a regex is used to extract the exact field header
            ing_re = re.match("ingredient\d+(.+)$", field)
            if ing_re.group(1) == IngredientAttribute.NAME.value:
                i.name = form[field]
            elif ing_re.group(1) == IngredientAttribute.ORIGIN.value:
                i.origin = form[field]
            elif ing_re.group(1) == IngredientAttribute.GMO.value:
                i.has_gmo=bool(form[field])
            elif ing_re.group(1) == IngredientAttribute.ORGANIC.value:
                i.is_organic=bool(form[field])
            elif ing_re.group(1) == IngredientAttribute.FAIR_FISHING.value:
                i.fair_fishing=bool(form[field])
            # If all the fields of the ingredient have been parsed, it should be added to the datasheet list
            if counter%len(IngredientAttribute) == 0:
                d.add_ingredient(copy(i))
                counter = 0
    return d
            #print(field+" : "+request.form[field])
