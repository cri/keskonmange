# keskonmange

Ce projet vise à exploiter les données de l'open data de la ville de Toulouse, en particulier concernant les menus des cantines scolaires.

# Mise en place

Je conseille d'utiliser un virtualenv pour ne pas s'embêter avec les versions de pythons des trouzemilles librairies du système. La commande `virtualenv -p <exécutable> venv` crée un environnement virtuel dans le dossier venv, avec l'interpréteur python que vous avez choisi en exécutable (typiquement, /usr/bin/python3). De là, vous pouvez activer l'environnement avec la commande `source venv/bin/activate`. Dorénavant, python et ses utilitaires sont résolus localement, et non au niveau système. Vous pouvez donc exécuter `pip install -r requirements.txt` pour installer les librairies listées dans requirements.txt. Si vous rajoutez des librairies au projet, pensez à les y inscrire.
