import requests
import json

menus_r = requests.get('https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=menus&rows=5000&facet=datemenu&facet=libelle_repas&facet=libelle_ordre_plat&facet=libelle_population_cible&sort=datemenu')
print("Menu request status code: "+str(menus_r.status_code))
with open("menus.json", "w") as menu_f:
    menu_f.write(json.dumps(menus_r.json(), indent=2))

recipe_r = requests.get('https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=plats-extrait-de-datameal&rows=5000')
print("Recipes request status code: "+str(recipe_r.status_code))
with open("recipes.json", "w") as recipe_f:
    recipe_f.write(json.dumps(recipe_r.json(), indent=2))

food_r = requests.get('https://data.toulouse-metropole.fr/api/records/1.0/search/?dataset=denrees-extrait-de-datameal&facet=libelle_recette&facet=libelle_denree&facet=arachide&facet=ovoproduit&facet=lait&facet=mollusques&facet=gluten&facet=fruits_a_coque&facet=sulfites&facet=crustaces&facet=sesame&facet=lupin&facet=poisson&facet=soja&facet=celeri&facet=moutarde&rows=5000')
print("Recipes request status code: "+str(food_r.status_code))
with open("food.json", "w") as food_f:
    food_f.write(json.dumps(food_r.json(), indent=2))
